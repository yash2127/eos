/* Function that only load video source on click */
const videoLoadOnClick = () => { // eslint-disable-line no-unused-vars
  $('.js-launch-modal').on('click', function () {
    /* Check which video we should load  */
    const videoToLoad = $(this).attr('data-video-url')

    /* Add the src attribute to the video element */
    $('.js-video-src').attr('src', videoToLoad) // eslint-disable-line no-unused-vars
    /* This is needed for "load" the video once the src is set */
    $('.js-video-modal video')[0].load()
  })
}
